<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterRequest;
use App\Services\EmailsParser;

class FilterController extends Controller
{
    public function storeFile(FilterRequest $request)
    {
        $parser = new EmailsParser();
        $parser->storeFile($request);
        $parser->startParsingProcess();
    }
}
