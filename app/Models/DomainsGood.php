<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainsGood extends Model
{
    use HasFactory;

    protected $table = 'domain_good';
    protected $fillable = [
        'domain'
    ];
}
