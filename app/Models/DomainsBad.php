<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomainsBad extends Model
{
    use HasFactory;

    protected $table = 'domain_bad';
    protected $fillable = [
        'domain'
    ];
}
