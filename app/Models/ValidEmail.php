<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValidEmail extends Model
{
    use HasFactory;

    protected $table = 'valid_emails';
    protected $fillable = [
        'email'
    ];
}
