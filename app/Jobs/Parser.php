<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\EmailsParser;
use App\Services\EmailsFilter;

class Parser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public string $fileName;

    /**
     * Create a new job instance.
     *
     * @param string $fileName
     */
    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function loadFile()
    {
        return file_get_contents(public_path($this->fileName));
    }

    public function parseFile(): array
    {
        $file = $this->loadFile();
        $parser = new EmailsParser();

        return $parser->parseFile($file);
    }

    public function getParsedFileDomains()
    {
        $parsedFile = $this->parseFile();
        $filter = new EmailsFilter();

        return $filter->getDomains($parsedFile);
    }

    public function runDomainsFilter()
    {
        $domains = $this->getParsedFileDomains();
        if (!$domains)
            DomainsFilter::dispatch($domains)->delay(now()->addSeconds(5));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->runDomainsFilter();
    }
}
