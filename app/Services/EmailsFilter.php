<?php


namespace App\Services;


class EmailsFilter
{
    public function getDomain(string $string)
    {
        $explode = explode('@', $string);
        if (!$explode || count($explode) > 0)
            return end($explode);

        return false;
    }

    public function getDomains(array $array)
    {
        $domains = [];
        foreach ($array as $item) {
            if (is_string($item)) {
                $domain = $this->getDomain($item);
                if ($domain)
                    $domains[] = $domain;
            }
        }

        $domains = array_unique(array_filter($domains));

        return count($domains) ? $domains : false;
    }
}
