<?php


namespace App\Services;


use App\Http\Requests\FilterRequest;
use App\Jobs\Parser;

class EmailsParser
{
    const PARSE_DONE = "File successfully parsed.";
    const PARSE_ERROR = "File can't be parsed or just empty.";

    protected string $fileName;

    /**
     * @param $file
     * @return array
     */
    public function parseFile($file): array
    {
        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $file, $matches);

        if (count($matches[0]) > 0)
            return ['status' => 'success', 'message' => self::PARSE_ERROR, 'emails' => $matches[0]];

        return ['status' => 'error', 'message' => self::PARSE_ERROR, 'emails' => []];
    }

    public function startParsingProcess($fileName = false)
    {
        if ($fileName)
            $this->fileName = $fileName;

        Parser::dispatch($this->fileName);
    }

    public function storeFile(FilterRequest $request)
    {
        $fileName = $request->file('emails')->store('/');
        $this->fileName = $fileName;
    }


}
